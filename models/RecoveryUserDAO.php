<?php

    require_once('SQLConnect.php');
    require_once('RecoveryUser.php');
    
    class RecoveryUserDAO {
        private static $dao;

        private function __construct() {

        }

        /**
         * Returns an instance of our DAO, following the singleton pattern
         * (we only want one FicheDAO active as a time)
         */
        public final static function getInstance() {
            if (!isset(self::$dao)) self::$dao = new RecoveryUserDAO();
            return self::$dao;
        }

        /**
         * Insert a new RecoveryUser in the table
         * @param st the instance of RecoveryUser (object created with init func) we want to insert
         */
        public final function insert($st) {
            if($st instanceof RecoveryUser) {
                $dbc = SQLConnect::getInstance()->getConnection();;
                // prepare the SQL statement
                $query = 'insert into recoveryuser(email, validationCode, createdAt) values (:em, :co, :ti)';
                $stmt = $dbc->prepare($query);
    
                // bind the attributes
                $stmt->bindValue(':em', $st->getEmail(), PDO::PARAM_STR);
                $stmt->bindValue(':co', $st->getValidationCode(), PDO::PARAM_STR);
                $stmt->bindValue(':ti', $st->getCreatedAt(), PDO::PARAM_STR);
    
                // execute the prepared statement
                $stmt->execute();
            }
        }

        /**
         * Search for a RecoveryUser by its email
         * @param email the email of the RecoveryUser
         * @return result the RecoveryUser
         */
        public final function findByMail($email) {
            $db = SQLConnect::getInstance()->getConnection();;
            $query = "SELECT * FROM recoveryuser WHERE email = :e";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }

        /**
         * Update a RecoveryUser with a new code, following its email
         * @param email of the RecoveryUser we want to update
         * @param newCode the new code of the RecoveryUser
         */
        public final function updateCodeByMail($email, $newCode) {
            $db = SQLConnect::getInstance()->getConnection();;
            $query = "UPDATE recoveryuser SET validationCode = :nc WHERE email = :e";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->bindValue(':nc', $newCode, PDO::PARAM_STR);
            $stmt->execute();
        }

        /**
         * Delete a RecoveryUser by its email
         * @param email the email of the RecoveryUser to delete
         */
        public final function deleteByMail($mail) {
            $db = SQLConnect::getInstance()->getConnection();;
            $query = "DELETE FROM recoveryuser WHERE email = :e";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':e', $mail, PDO::PARAM_STR);
            $stmt->execute();
        }

        /**
         * Search for a RecoveryUser's email by its code
         * @param code the code of the RecoveryUser
         * @return result the RecoveryUser's email
         */
        public final function findEmailByCode($code) {
            $db = SQLConnect::getInstance()->getConnection();;
            $query = "SELECT email FROM recoveryuser WHERE validationCode = :c";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':c', $code, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
        }

        /**
         * Search for a RecoveryUser by its code
         * @param code the code of the RecoveryUser
         * @return result the RecoveryUser
         */
        public final function findByCode($code) {
            $db = SQLConnect::getInstance()->getConnection();;
            $query = "SELECT * FROM recoveryuser WHERE validationCode = :c";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':c', $code, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }


    }