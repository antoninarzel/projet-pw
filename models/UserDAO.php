<?php

    require_once('SQLConnect.php');
    require_once('User.php');

    class UserDAO {
        private static $dao;

        private function __construct() {

        }

        /**
         * Returns an instance of our DAO, following the singleton pattern
         * (we only want one UserDAO active as a time)
         */
        public final static function getInstance() {
            if (!isset(self::$dao)) self::$dao = new UserDAO();
            return self::$dao;
        }

        public final function findAll() {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'SELECT * FROM user';
            $stmt = $dbc->prepare($query);
            $stmt->execute();
            $arr = $stmt->fetchAll();
            if (!$arr) exit('No rows!');
            var_export($arr);
            $stmt = null;
        }

        /**
         * Search for a User by its code
         * @param code the code of the User
         * @return result the User
         */
        public final function findByCode(string $code) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'SELECT username FROM user WHERE validationCode = :cod';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':cod', $code, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll();
            return $res;
        }

        /**
         * Search for a User by its username
         * @param username the username of the User
         * @return result the User
         */
        public final function findByUsername(string $username) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'SELECT username FROM user WHERE username = :un';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':un', $username, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll();
            return $res;
        }

        /**
         * Search for a User by its email
         * @param email the email of the User
         * @return result the User
         */
        public final function findByMail(string $email) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'SELECT email FROM user WHERE email = :e';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll();
            return $res;
        }

        /**
         * Insert a new User in the table
         * @param user the instance of User (object created with init func) we want to insert
         */
        public final function insert($user) {
            if ($user instanceof User) {
                $dbco = SQLConnect::getInstance()->getConnection();
                $query = 'INSERT INTO user(firstname, lastname, username, email, password, birthday, ip, isValid, validationCode) VALUES (:fn, :ln, :un, :e, :pw, :bd, :ip, :val, :cod)';
                $stmt = $dbco->prepare($query);
                $stmt->bindValue(':fn', $user->getFirstName(), PDO::PARAM_STR);
                $stmt->bindValue(':ln', $user->getLastName(), PDO::PARAM_STR);
                $stmt->bindValue(':un', $user->getUserName(), PDO::PARAM_STR);
                $stmt->bindValue(':e', $user->getEmail(), PDO::PARAM_STR);
                $stmt->bindValue(':pw', $user->getPassword(), PDO::PARAM_STR);
                $stmt->bindValue(':bd', $user->getBirthday(), PDO::PARAM_STR);
                $stmt->bindValue(':ip', $user->getIp(), PDO::PARAM_STR);
                $stmt->bindValue(':val', $user->getIsValid(), PDO::PARAM_STR);
                $stmt->bindValue(':cod', $user->getValidationCode(), PDO::PARAM_STR);
                $stmt->execute();
            }
        }

        /**
         * Validate and account following it code
         * @param code of the user
         */
        public final function validate(string $code) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'UPDATE user SET isValid = 1 WHERE validationCode = :cod';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':cod', $code, PDO::PARAM_STR);
            $stmt->execute();
        }

        /**
         * Delete the code of an account (the user has validated its account)
         * @param code of the user
         */
        public final function deleteCode(string $code) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'UPDATE user SET validationCode = 0 WHERE validationCode = :cod';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':cod', $code, PDO::PARAM_STR);
            $stmt->execute();
        }

        public function update($email, $newValue) {
            $db = SQLConnect::getInstance()->getConnection();
            $query = "UPDATE user SET $attribute = :val WHERE email = :e";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':val', $newValue, PDO::PARAM_STR);
            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->execute();
        }
      
      /**
      	* Updates the User password.
        * @param email the email of the user
        * @param newValue the new password
        */
      public function updatePassword($email, $attribute, $newValue) {
            $db = SQLConnect::getInstance()->getConnection();
            $query = "UPDATE user SET password = :val WHERE email = :e";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':val', $newValue, PDO::PARAM_STR);
            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->execute();
        }
        
        /**
         * Returns if the User account has been validated or not
         * @param email the email of the user
         * @return result 1 if validated, otherwise 0
         */
        public function isValid($email) {
          $db = SQLConnect::getInstance()->getConnection();
          $query = "SELECT isValid FROM user WHERE email = '$email'";
          $stmt = $db->prepare($query);
          $stmt->execute();
          $result = $stmt->fetchColumn();
          return $result;
      	}
          
          /**
         * Search for a User password by its email
         * @param email the email of the User
         * @return result the User
         */
      	public function getPasswd($email) {
          $db = SQLConnect::getInstance()->getConnection();
          $query = "SELECT password FROM user WHERE email = '$email'";
          $stmt = $db->prepare($query);
          $stmt->execute();
          $result = $stmt->fetchColumn();
          return $result;
        }
        
        /**
         * Search for a User id by its email
         * @param email the email of the User
         * @return result the User
         */
        public function getId($email) {
          $db = SQLConnect::getInstance()->getConnection();
          $query = "SELECT id FROM user WHERE email = '$email'";
          $stmt = $db->prepare($query);
          $stmt->execute();
          $result = $stmt->fetchColumn();
          return $result;
      	}   
          
          /**
         * Search for a User firstname by its email
         * @param email the email of the User
         * @return result the User
         */
        public function getFirstname($email) {
            $db = SQLConnect::getInstance()->getConnection();
            $query = "SELECT firstname FROM user WHERE email = '$email'";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
          }
          
          /**
         * Search for a User username by its email
         * @param email the email of the User
         * @return result the User
         */
      	public function getUsernameWithMail($email) {
          $db = SQLConnect::getInstance()->getConnection();
          $query = "SELECT username FROM user WHERE email = '$email'";
          $stmt = $db->prepare($query);
          $stmt->execute();
          $result = $stmt->fetchColumn();
          return $result;
        }
    }