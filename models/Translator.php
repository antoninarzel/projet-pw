<?php
    /**
     * Translator class goal is to translate every text to another lang.
     * It is really simple to understand : every strings are stored in a JSON file with a key -> value presentation
     * For instance we have :
     * (fr.json) "home_title" : "Page d'accueil"
     * (en.json) "home_title" : "Homepage"
     * The functions we have here are only made for reading the JSON files.
     */
    class Translator {
        private $lang;

        /**
         * Creates the Translator object and associate a lang.
         * @param lang the lang
         */
        public function __construct($lang) {
            $this->setLang($lang);
        }

        /**
         * Returns the lang of the Translator
         * @return lang the lang
         */
        public function getLang() : string {
            return $this->lang;
        }

        /**
         * Sets the lang of the Translator
         * @param lang the lang
         */
        public function setLang(string $lang) {
            $this->lang = $lang;
        }

        /**
         * Reads the JSON file and returns its content
         * @return json_data the content of the translation file
         */
        public function readFile() {
            $readPath = 'fr.json';
            if ($this->lang === 'en') $readPath = 'en.json';
            if ($this->lang === 'es') $readPath = 'es.json';
            $json = @file_get_contents('./models/lang/'.$readPath);
            $json_data = '';
            if ($json !== FALSE) $json_data = json_decode($json, true);
            return $json_data;
        }
        
        /**
         * Reads the JSON file to find the value associated to a specific key.
         * That's the function we always use on the views to print our text in multiple languages.
         * @param key the key to read
         * @return json_data the value associated to the parameter key
         */
        public function readKey($key) {
            $json_data = $this->readFile();
            if (isset($json_data[$key])) return $json_data[$key];
            else return 'Translation error (missing key)';
        }
    }