<?php

    class RecoveryUser {
        private $email;
        private $validationCode;
        private $createdAt;
        
        public function __construct() {
            
        }
		
        /**
         * Defines the attribute of the RecoveryUser object.
         */
        public function init($email, $validationCode, $createdAt) {
            $this->setEmail($email);
            $this->setValidationCode($validationCode);
            $this->setCreatedAt($createdAt);
        }

        /**
         * Getters and setters for all attributes
         */
        public function getEmail() : string { return $this->email; }
        public function setEmail(string $email) { $this->email = $email; }

        public function getValidationCode() : string { return $this->validationCode; }
        public function setValidationCode(string $validationCode) { $this->validationCode = $validationCode; }

        public function getCreatedAt() : string { return $this->createdAt; }
        public function setCreatedAt(string $createdAt) { $this->createdAt = $createdAt; }
        
    }