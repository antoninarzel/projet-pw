<?php
    class fiche {

        private $title;
        private $content; //stocker les données (texte, images...) en HTML
      	private $user_id;
      	private $code;
       

        public function __construct(){
            
        }
		
      
        /**
         * Defines the attribute of the Fiche object.
         */
        public function init($title, $content, $user_id, $code){
          $this->setTitle($title);
          $this->setContent($content);
          $this->setUserId($user_id);
          $this->setCode($code);
        }
        
        /**
         * Getters and setters for all attributes
         */
      	public function getTitle() : string { return $this->title; }
        public function setTitle(string $title) { $this->title = $title; }
      
      	public function getContent() : string { return $this->content; }
        public function setContent(string $content) { $this->content = $content; }
      
      	public function getUserId() { return $this->user_id; }
        public function setUserId($user_id) { $this->user_id = $user_id; }
      
      	public function getCode() : string { return $this->code; }
        public function setCode(string $code) { $this->code = $code; }


    }