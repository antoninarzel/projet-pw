<?php
require_once('SQLConnect.php');
    require_once('Fiche.php');

    class FicheDAO {
        private static $dao;

        private function __construct() {

        }

        /**
         * Returns an instance of our DAO, following the singleton pattern
         * (we only want one FicheDAO active as a time)
         */
        public final static function getInstance() {
            if (!isset(self::$dao)) self::$dao = new FicheDAO();
            return self::$dao;
        }

        /**
         * Search for a fiche by its id
         * @param id the id of the fiche
         * @return res the fiche
         */
        public final function findByid(int $id) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'SELECT * FROM fiche WHERE user_id = :id';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $res = $stmt->fetchAll();
            return $res;
        }

        /**
         * Search for a fiche by its title
         * @param id the title of the fiche
         * @return res the fiche
         */
        public final function findByTitle(string $title, int $id) {
            $dbco = SQLConnect::getInstance()->getConnection();
            $query = 'SELECT * FROM fiche WHERE idUser = :id AND title = :title';
            $stmt = $dbco->prepare($query);
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->bindValue(':title', $title, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll();
            return $res;
        }
        
        /**
         * Delete a fiche by its code
         * @param code the code of the fiche to delete
         */
      	public final function delete($code) {
          $dbc = SQLConnect::getInstance()->getConnection();
          $query = "DELETE FROM fiche WHERE code = '$code'";
          $sth = $dbc->prepare($query);
          $sth->execute();
        }

        /**
         * Insert a new fiche in the table
         * @param st the instance of fiche (object created with init func) we want to insert
         */
        public final function insert($st) {
            if($st instanceof Fiche) {
                $dbc = SQLConnect::getInstance()->getConnection();
                // prepare the SQL statement
                $query = 'insert into fiche(title, content, user_id, code) values (:t, :c, :u, :cod)';
                $stmt = $dbc->prepare($query);
    
                // bind the attributes
                $stmt->bindValue(':t', $st->getTitle(), PDO::PARAM_STR);
                $stmt->bindValue(':c', $st->getContent(), PDO::PARAM_STR);
                $stmt->bindValue(':u', $st->getUserId(), PDO::PARAM_STR);
              	$stmt->bindValue(':cod', $st->getCode(), PDO::PARAM_STR);
    
                // execute the prepared statement
                $stmt->execute();
            }
        }
        
        /**
         * Update a fiche with a new content, following its code
         * @param code of the fiche we want to update
         * @param content the new content of the fiche
         */
      	public final function updateContent($code, $content) {
            $db = SQLConnect::getInstance()->getConnection();;
            $query = "UPDATE fiche SET content = :nc WHERE code = :cod";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':nc', $content, PDO::PARAM_STR);
            $stmt->bindValue(':cod', $code, PDO::PARAM_STR);
            $stmt->execute();
        }
        
        /**
         * Update a fiche with a new title, following its code
         * @param code of the fiche we want to update
         * @param title the new content of the fiche
         */
        public final function updateTitle($code, $title) {
              $db = SQLConnect::getInstance()->getConnection();;
              $query = "UPDATE fiche SET title = :t WHERE code = :cod";
              $stmt = $db->prepare($query);
              $stmt->bindValue(':t', $title, PDO::PARAM_STR);
              $stmt->bindValue(':cod', $code, PDO::PARAM_STR);
              $stmt->execute();
          }
        
          /**
           * Gets the content of a fiche
           * @param code of the fiche
           */
        public function getContent($code) {
            $db = SQLConnect::getInstance()->getConnection();
            $query = "SELECT content FROM fiche WHERE code = '$code'";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchColumn();
            return $result;
          }
          
          /**
           * Gets the title of a fiche
           * @param code of the fiche
           */
        public function getTitleByCode($code) {
              $db = SQLConnect::getInstance()->getConnection();
              $query = "SELECT title FROM fiche WHERE code = '$code'";
              $stmt = $db->prepare($query);
              $stmt->execute();
              $result = $stmt->fetchColumn();
              return $result;
        }
      	
        /**
         * Returns the number of sheets created by an user.
         *	@param user the user we want to targer (by its user id)
        *  @return the number of its sheets
         */
      	public final function nbFiches($user) {
           $dbco = SQLConnect::getInstance()->getConnection();
           $query = 'SELECT COUNT(*) FROM fiche WHERE user_id = :usr';
          $stmt = $dbco->prepare($query);
          $stmt->bindValue(':usr', $user, PDO::PARAM_INT);
          $stmt->execute();
          $res = $stmt->fetchColumn();
          return $res;   
        }

    }