<?php

    ini_set('display_errors', 'On');
    error_reporting(E_ALL);
    
    class SQLConnect {
        private static $pdo;

        public function __construct() {}

        public final static function getInstance() {
            if (!isset(self::$pdo)) self::$pdo = new SQLConnect();
            return self::$pdo;
        }
		
        /**
         * Function that instanciate a connexion with our database.
         */
        public function getConnection() {
            // To-do : replace credentials
            // To-do : secure credentials (in .env file or else)
            $host_name = 'db5001418286.hosting-data.io';
            $database = 'dbs1196510';
            $user_name = 'dbu1162152';
          	$pwd = 'pt2t4S$!v#UbLH+';
            try {
                $pdo_connect = new PDO('mysql:host='.$host_name.';dbname='.$database.'', $user_name, $pwd);
                $pdo_connect->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                $pdo_connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $pdo_connect->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                print 'Erreur ! '. $e->getMessage() . "<br/>";
                die();
            }
            return $pdo_connect;
        }
    }