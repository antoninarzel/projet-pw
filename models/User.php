<?php

    class User {
        private $firstname;
        private $lastname;
        private $username;
        private $email;
        private $password;
        private $birthday;
        private $ip;
        private $isValid;
        private $validationCode;
        
        public function __construct() {
            
        }
        
        /**
         * Defines the attribute of the User object.
         */
        public function init($firstname, $lastname, $username, $email, $password, $birthday, $ip, $isValid, $validationCode) {
            $this->setFirstName($firstname);
            $this->setLastName($lastname);
            $this->setUserName($username);
            $this->setEmail($email);
            $this->setPassword($password);
            $this->setBirthday($birthday);
            $this->setIp($ip);
            $this->setIsValid($isValid);
            $this->setValidationCode($validationCode);
        }

        /**
         * Getters and setters for all attributes
         */
        public function getFirstName() : string { return $this->firstname; }
        public function setFirstName(string $firstname) { $this->firstname = $firstname; }

        public function getLastName() : string { return $this->lastname; }
        public function setLastName(string $lastname) { $this->lastname = $lastname; }

        public function getUserName() : string { return $this->username; }
        public function setUserName(string $username) { $this->username = $username; }

        public function getEmail() : string { return $this->email; }
        public function setEmail(string $email) { $this->email = $email; }

        public function getPassword() : string { return $this->password; }
        public function setPassword(string $password) { $this->password = $password; }

        public function getBirthday() : string { return $this->birthday; }
        public function setBirthday(string $birthday) { $this->birthday = $birthday; }

        public function getIp() : string { return $this->ip; }
        public function setIp(string $ip) { $this->ip = $ip; }

        public function getIsValid() : int { return $this->isValid; }
        public function setIsValid(int $isValid) { $this->isValid = $isValid; }

        public function getValidationCode() : string { return $this->validationCode; }
        public function setValidationCode(string $validationCode) { $this->validationCode = $validationCode; }
    }