<?php 

    require('controllers/Controller.php');
    require('models/SQLConnect.php');
    require_once('models/UserDAO.php');
    require_once('models/FicheDAO.php');
    require_once('models/User.php');
    require_once('models/Fiche.php');

    class SheetController implements Controller {
        public function handle($requet) {
			if (isset($_GET['code'])) {
            	if (isset($_GET['mode'])) {
                  if ($_GET['mode'] === "edit") {
                    if (isset($_POST['submit'])) {
                      $code = $_GET['code'];
                      $content = $_POST['editor'];
                      $title = htmlspecialchars($_POST['title']);
                      $content_l = strlen($content);
                      if ($content_l >= 5000) {
                        $_SESSION['code'] = 'over_5000char';
                      } else {
                        $ficheDAO = FicheDAO::getInstance();
                        $ficheDAO->updateContent($code, $content);
                        $ficheDAO->updateTitle($code, $title);
                        $content = $ficheDAO->getContent($code);
                        $title = $ficheDAO->getTitleByCode($code);
                        $_SESSION['content'] = $content;
                        $_SESSION['title'] = $title;
                      }
                      header('Location: index.php?page=home');
                    } else {
                      $code = $_GET['code'];
                      $ficheDAO = FicheDAO::getInstance();
                      $content = $ficheDAO->getContent($code);
                      $title = $ficheDAO->getTitleByCode($code);
                      $_SESSION['content'] = $content;
                      $_SESSION['title'] = $title;
                      //header('Location: index.php?page=home');
                    }
                  } else if ($_GET['mode'] === "delete") {
                    	if (isset($_GET['code'])) {
                          $code = $_GET['code'];
                          $ficheDAO = FicheDAO::getInstance();
                          $ficheDAO->delete($code);
                          header('Location: index.php?page=home');
                        } else {
                          header('Location: index.php?page=home');
                        }
                    	$code = $_GET['code'];
                    	
                  }                  
                } else {
              		header('Location: index.php?page=home');
                } 
            } else {
            	header('Location: index.php?page=home'); 
            }

        }

    }