<?php 

    require('controllers/Controller.php');
    require('models/SQLConnect.php');
    require_once('models/UserDAO.php');
    require_once('models/RecoveryUserDAO.php');
    require_once('models/User.php');
    require_once('models/RecoveryUser.php');

    class RecoveryController implements Controller {
        public function handle($requet) {
            if (isset($_POST['submit'])) {
                $email = $_POST['email'];
                if (!is_null($email) && !empty($email)) {
                    $email = htmlspecialchars($email);
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $userdao = UserDAO::getInstance();
                      	$username = $userdao->getUsernameWithMail($email);
                        $mailExists = $userdao->findByMail($email);
                        if (count($mailExists) == 1) {
                            if ($userdao->isValid($email)) {
                                // Le mail existe bien dans les utilisateurs
                                // On créé un RecoveryUtilisateur
                                $_SESSION['sendTo'] = $email;
                                $validationCode = bin2hex(random_bytes(6));
                                $_SESSION['validationCode'] = $validationCode;
                                $_SESSION['username'] = $username; 
                                $recoveryUserDAO = RecoveryUserDAO::getInstance();
                                $mailRecupExists = $recoveryUserDAO->findByMail($email);
                                if (count($mailRecupExists)) {
                                    $time = date('Y-m-d H:i:s');
                                    $recoveryUserDAO->updateCodeByMail($email, $validationCode, $time);
                                } else {
                                    $time = date('Y-m-d H:i:s');
                                    $recoveryUser = new RecoveryUser();
                                    $recoveryUser->init($email, $validationCode, $time);
                                    $recoveryUserDAO->insert($recoveryUser);
                                }
                              require('models/mail/MailRecovery.php');
                            	$_SESSION['code'] = 'recovery_success0';
                            } else {
                                $_SESSION['code'] = 'recovery_err3';
                                // Erreur : le compte doit d'abord être validé
                            }
                            // On envoie ensuite l'email
                        } else {
                            $_SESSION['code'] = 'recovery_err0';
                            // Erreur : aucun utilisateur n'existe avec cette adresse mail
                        }
                    } else {
                        $_SESSION['code'] = 'recovery_err1';
                        // Erreur : l'email ne respecte pas le format requis
                    }
                } else {
                    $_SESSION['code'] = 'recovery_err2';
                    // Erreur : le champ 'email' ne peut pas être vide
                }
            }
        }
    }