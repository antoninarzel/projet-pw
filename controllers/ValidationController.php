<?php 

    require('controllers/Controller.php');
    require('models/SQLConnect.php');
    require_once('models/UserDAO.php');
    require_once('models/User.php');

    class ValidationController implements Controller {
        public function handle($requet) {
            if (isset($_GET['code'])) {
                $code =  $_GET['code'];
                if ($code != '0') {
                    $userDAO = UserDAO::getInstance();
                    $checkCode = $userDAO->findByCode($code);
                    if (count($checkCode) === 1) {
                        $userDAO->validate($code);
                        $userDAO->deleteCode($code);
                        $_SESSION['code'] = 'valid1';
                        header('Location: index.php?page=login');
                    } else {
                        header('Location: index.php?page=error');
                    }
                } else {
                    header('Location: index.php?page=error');
                }     
            } else {
                header('Location: index.php?page=error');
            }
        }



    }