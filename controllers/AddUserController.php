<?php
    require('controllers/Controller.php');
    require('models/SQLConnect.php');
    require_once('models/UserDAO.php');
    require_once('models/User.php');
    class AddUserController implements Controller {
        public function handle($request) {
            if (isset($_POST['submit'])) {
                // Verif : all fields are set and not empty
                $firstname = htmlspecialchars($_POST['firstname']);
                $name = htmlspecialchars($_POST['name']);
                $username = htmlspecialchars($_POST['username']);
                $birthday = $_POST['birthday'];
                $email = $_POST['email'];
                $password = $_POST['password'];
              	$ip = $_SERVER['REMOTE_ADDR'];
                $isValid = 0;
                // To-do : verif params length between min and max
                if (!is_null($firstname) && !empty($firstname)) {
                    if (!is_null($name) && !empty($name)) {
                        if (!is_null($username) && !empty($username)) {
                            if (!is_null($birthday) && !empty($birthday)) {
                                if (!is_null($email) && !empty($email)) {
                                    if (!is_null($password) && !empty($password)) {
                                        $password = password_hash($password, PASSWORD_BCRYPT);
                                        $userDAO = UserDAO::getInstance();
                                        $isUsernameUsed = $userDAO->findByUsername($username);
                                        if (count($isUsernameUsed) == 0) {
                                            $isEmailUsed = $userDAO->findByMail($email);
                                            if (count($isEmailUsed) == 0) {
                                                $validationCode = bin2hex(random_bytes(6));
                                                $user = new User();
                                                $user->init($firstname, $name, $username, $email, $password, $birthday, $ip, $isValid, $validationCode);
                                                $userDAO->insert($user);
                                                $_SESSION['code'] = 'valid0';
                                                $_SESSION['sendTo'] = $email;
                                                $_SESSION['username'] = $username;     
                                                $_SESSION['validationCode'] = $validationCode;
                                                require('models/mail/MailValidation.php');
                                            } else {
                                                // To-do : manage errcodes
                                                $_SESSION['code'] = 'err0';
                                             }
                                        } else {
                                            $_SESSION['code'] = 'err1';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
?>