<?php
    require('controllers/Controller.php');
    require('models/SQLConnect.php');
    require_once('models/UserDAO.php');
    require_once('models/User.php');
    class ConnectUserController implements Controller {
        public function handle($request) {
            

            if (isset($_POST['signin'])) {
                if (!empty($_POST['email']) && !empty($_POST['password'])) {
                    $email = $_POST['email'];
                    $email = stripslashes($email);
                    $password = $_POST['password'];
                    $password = stripslashes($password);
                    $userDAO = UserDAO::getInstance();
                  	if (password_verify($password, $userDAO->getPasswd($email))) {
                      if ($userDAO->isValid($email)) {
                         	$_SESSION['userid'] = $userDAO->getId($email);
                        	$_SESSION['firstname'] = $userDAO->getFirstname($email);
                            $_SESSION['logged'] = 'on';
                            header('Location: index.php?page=home');
                      } else {
                        $_SESSION['err_co_code'] = '0';
                        // Votre compte n'a pas encore été validé.
                      }
                    } else {
                      $_SESSION['err_co_code'] = '1';
                      // L'adresse mail et le mot de passe ne correspondent pas.
                    }
                }
            }
        }
    }
?>