<?php

    require('controllers/Controller.php');
    require('models/SQLConnect.php');
    require('models/UserDAO.php');
    require('models/RecoveryUserDAO.php');
    require_once('models/User.php');
    require_once('models/RecoveryUser.php');

    class RecoverController implements Controller {
        public function handle($request) {
            $code = $_GET['code'];
            $recoveryUserDAO = RecoveryUserDAO::getInstance();
            $email = $recoveryUserDAO->findEmailByCode($code);
          
            if (strlen($email)) {
                if (isset($_POST['submit'])) {
                    if (isset($_POST['password']) && isset($_POST['password_two'])) {
                        $password = $_POST['password'];
                        $password_two = $_POST['password_two'];
                        if ((!is_null($password) && !empty($password)) || (!is_null($password_two) && !empty($spassword_two))) {
                            if ($password === $password_two) {
                                $password = password_hash($password, PASSWORD_BCRYPT);
                                $userDAO = UserDAO::getInstance();
                                $userDAO->updatePassword($email, 'password', $password);
                                $recoveryUserDAO->deleteByMail($email);
                                $_SESSION['code'] = 'recover_success0';
                            } else {
                                $_SESSION['code'] = 'recover_err0';
                                // Erreur : les deux mots de passes ne correspondent pas.
                            } 
                        } else {
                            $_SESSION['code'] = 'recover_err1';
                            // Erreur : mot de passe vide.
                        }
                    } else {
                        $_SESSION['code'] = 'recover_err2';
                        // Erreur : mot de passe non définis.
                    }
                }
            } else {
                header('Location: https://mypost-it.fr/index.php?page=error');
            }
        }
    }