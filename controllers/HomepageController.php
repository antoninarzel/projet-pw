<?php

    require('controllers/Controller.php');
	require('models/SQLConnect.php');
    require_once('models/FicheDAO.php');
    require_once('models/Fiche.php');
    

    class HomepageController implements Controller {
        public function handle($request) {
            if (isset($_SESSION['logged'])) {
              $ficheDAO = FicheDAO::getInstance();
              $nbfiche = $ficheDAO->nbFiches($_SESSION['userid']);
              $_SESSION['nb_fiche'] = $nbfiche;
              $content = $ficheDAO->findByid($_SESSION['userid']);
			  $_SESSION['fiches'] = $content;
              if (isset($_POST['add_sheet'])) {
                $sheet_code = bin2hex(openssl_random_pseudo_bytes(10));
                $fiche = new Fiche();
                $translator = new Translator($_SESSION['lang']);
                $fiche->init($translator->readKey('set_title'), '<span style="font-size: 12pt; color: #000; font-family: verdana, geneva, sans-serif;">'.$translator->readKey('set_content').'</span>', $_SESSION['userid'], $sheet_code);
                $ficheDAO->insert($fiche);
                header('Location: index.php?page=sheet&mode=edit&code='.$sheet_code);
              }
            }
            else {
                header('Location: index.php?page=login');
            }

        }
    }