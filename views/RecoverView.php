<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=$translator->readKey('reset_title');?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="resources/css/style.css">
        <link rel="icon" href="resources/images/favicon.ico" />
        <script src="vendor/components/jquery/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
      <div class="main">
        <section class="sign-in">
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="resources/images/signin-image.jpg" alt="sing up image"></figure>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title"><?=$translator->readKey('reset_message');?></h2>
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-lock"></i></label>
                              <input id="password" name="password" type="password" onchange="this.setCustomValidity(this.validity.patternMismatch ? "<?=$translator->readKey('custom_pw1');?>" : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" placeholder="<?=$translator->readKey('password');?>" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                            </div>
                            <div class="form-group">
                                <label for="password"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input id="password_two" name="password_two" type="password" onchange="this.setCustomValidity(this.validity.patternMismatch ? '<?=$translator->readKey('custom_pw2');?>' : '');" placeholder="<?=$translator->readKey('verify_password');?>" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="submit" id="submit" class="form-submit" value="<?=$translator->readKey('reset_message');?>" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php 
        if (isset($_SESSION['code'])) {
            if ($_SESSION['code'] == 'recover_success0') {
              echo '<script>swal({icon:"success",title:"'.$translator->readKey('success_message').'",showConfirmButton: false,text:"'.$translator->readKey('success_reset_pw').'"}).then(function(){window.location="index.php?page=login"})</script>';
            }
        }
        $_SESSION['code'] = null;
    ?>
    </body>
</html>