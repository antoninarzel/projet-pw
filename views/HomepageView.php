<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<title><?=$translator->readKey('disconnect_title');?></title>
  	<link rel="icon" href="resources/images/favicon.ico" />
  	<link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css" />
	<link rel="stylesheet" href="resources/css/style.css" />
  	<link rel="stylesheet" href="resources/css/bootstrap.css" />
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
  <div class="container mt-4 mb-4">
  	<div class="row justify-content-md-center">
		<div class="col-md-12 col-lg-8 mt-4 p-3">
        	<?php if(isset($_SESSION['firstname'])) echo '<h1>Bonjour '.ucfirst($_SESSION['firstname']).',</h1>' ?>
          <a href="resources/php/session_destroyer.php"><?=$translator->readKey('logout');?></a>
    	</div>
  	</div>
    <div class="row justify-content-md-center">
      <div class="col-md-12 col-lg-8 mt-4 p-3">
        <h6><?=$translator->readKey('welcome_home');?></h6>
        <p><?=$translator->readKey('welcome_msg');?></p>
      </div>
    </div>
    <div class="row justify-content-md-center">
    	<div class="col-md-4 col-sm-4 col-xs-4">
           <form method="post">
         	<input type="submit" class="btn btn-primary btn-block" name="add_sheet" value="<?=$translator->readKey('create_record');?>">
           </form>
      	</div>
    </div>
    <div class="row justify-content-md-center">
      <div class="col-md-12 col-lg-8 mt-4 p-3">
        <h5><?=$translator->readKey('record_list');?>
        <?php 
  			if (isset($_SESSION['nb_fiche'])) {
              if ($_SESSION['nb_fiche'] == 0) echo "&nbsp;".$translator->readKey('no_record')."</h5>";
              else if ($_SESSION['nb_fiche'] == 1) echo "&nbsp;".$translator->readKey('one_record')."</h5>";
              else echo "&nbsp;(".$_SESSION['nb_fiche']."".$translator->readKey('more_records')."</h5>";
            } else {
              echo "</h5>";
            }
		?>
          </div>
      </div>
        <?php
          for ($i = 0; $i < $_SESSION['nb_fiche']; $i++) {
            $arr = $_SESSION['fiches'][$i];
            $print = '<a style="text-decoration:none;" href="index.php?page=sheet&mode=edit&code='.$arr['code'].'">';
            $delete = '<a style="text-decoration:none;" href="index.php?page=sheet&mode=delete&code='.$arr['code'].'">';
            echo $print;
           	echo "<div class='row justify-content-md-center'> <div class='col-md-12 col-lg-8 mt-4 p-3'><div class='card sheet'> <div class='card-block p-3'><i style='color:red;display:block;float:right;'>".$delete."".$translator->readKey('delete')."</a></i>".$print."<h5 class='card-title'>".$arr['title']."</h5><p class='card-subtitle text-muted text-truncate'>".$arr['content']."</p></a></div></div></div></div></a>";
          }
          ?>
      </div>
    </div>
  </div>
</body>    
</html>