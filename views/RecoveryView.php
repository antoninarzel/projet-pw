<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$translator->readKey('forgotpw_title');?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="resources/css/style.css">
  	<link rel="icon" href="resources/images/favicon.ico" />
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
    <div class="main">
        <section class="sign-in">
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="resources/images/recover-pw.jpg" alt="sing up image"></figure>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title"><?= $translator->readKey("password_forgot"); ?></h2>
                        <p class="signup-image-link"><?= $translator->readKey("enter_mail"); ?></p>
                        <br>
                        <form method="post">
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" minlength="6" maxlength="50" placeholder="<?= $translator->readKey('insert_email'); ?>" name="email" id="email" required>
                            </div>
                            <div class="form-group-button">
                                <input type="submit" name="submit" id="submit" class="form-submit" value="<?= $translator->readKey('register_title'); ?>" />
                            </div>
                        </form>
                    </div>

                </div>
        </section>
    </div>
    <?php
    if (isset($_SESSION['code'])) {
        if ($_SESSION['code'] == 'recovery_success0') {
            echo '<script>swal({icon:"success",title:"'.$translator->readKey('success_message').'",showConfirmButton: false,text:"'.$translator->readKey('reset_pw_sent').'"})</script>';
        } else if ($_SESSION['code'] == 'recovery_err0') {
            echo '<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",text:"'.$translator->readKey('no_acc_found').'"})</script>';
        } else if ($_SESSION['code'] == 'recovery_err1') {
            echo '<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",text:"'.$translator->readKey('bad_mail_format').'"})</script>';
        } else if ($_SESSION['code'] == 'recovery_err2') {
            echo '<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",text:"'.$translator->readKey('no_mail').'"})</script>';
        } else if ($_SESSION['code'] == 'recovery_err3') {
            echo '<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",text:"'.$translator->readKey('must_be_validated').'"})</script>';
        }       
    }
    $_SESSION['code'] = null;
    ?>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <script src="" async defer></script>
</body>

</html>