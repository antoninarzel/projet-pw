<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=$translator->readKey('404_error_title');?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow">
        <link rel="icon" href="resources/images/favicon.ico" />
        <link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="resources/css/style.css">
        <link rel="icon" href="resources/images/favicon.ico" />
        <script src="vendor/components/jquery/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      <style>body{background:#cecece;}</style>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <script>swal({icon:"warning",title:"<?= $translator->readKey('error_message')?>", icon: 'resources/images/404-img.png', showConfirmButton: false,text:"<?= $translator->readKey('404_error') ?>"}).then(function(){window.location="index.php?page=login"})</script>
    </body>
</html>