<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
	<![endif]-->
<!--[if IE 7]>
	<html class="no-js lt-ie9 lt-ie8">
		<![endif]-->
<!--[if IE 8]>
		<html class="no-js lt-ie9">
			<![endif]-->
<!--[if gt IE 8]>
			<html class="no-js">
				<!--
				<![endif]-->
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?=$translator->readKey('addsheet_title');?></title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="robots" content="noindex, nofollow" />
	<link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
	<link rel="stylesheet" href="resources/css/style.css" />
	<link rel="icon" href="resources/images/favicon.ico" />
	<script src="vendor/components/jquery/jquery.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body onload="fillDocument()">
	<div class="container mt-4 mb-4">
		<div class="row justify-content-md-center">
			<div class="col-md-12 col-lg-8">
				<form method="post">
					<input class="h2 mb-4" name="title" type="text" maxlength="50" placeholder="<?=$translator->readKey('insert_title');?>" id="title"></input>
					<div class="form-group">
						<textarea id="editor" name="editor"></textarea>
					</div>
              		<center><button type="submit" class="btn btn-primary" name="submit"><?=$translator->readKey('save');?></button></center>
				</form>
			</div>
		</div>
	</div>
	<script src="https://cdn.tiny.cloud/1/e07sjx5pkowbci2ts02oofjk3zj0fsv95dmcr9sv16p5qelc/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
		var useDarkMode = window.matchMedia("(prefers-color-scheme: dark)").matches;
		            tinymce.init({
		                selector: "textarea#editor",
		                force_br_newlines : true,
		                force_p_newlines : false,
		                forced_root_block : '',
		                plugins:
		                    "print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons",
		                menubar: "file edit view format tools table tc help",
		                toolbar:
		                    "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl",
		                toolbar_sticky: true,
		                autosave_ask_before_unload: true,
		                autosave_interval: "30s",
		                autosave_restore_when_empty: false,
		                height: 600,
		                image_caption: true,
		                toolbar_mode: "sliding",
		                skin: useDarkMode ? "oxide-dark" : "oxide",
		                content_css: useDarkMode ? "dark" : "default",
		                content_style: "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
		            });
		            
		            function fillDocument() {
		            	tinymce.get("editor").setContent('<?= $_SESSION['content']; ?>');
		                document.getElementById('title').value = "<?= $_SESSION['title']; ?>";
		           }
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<?php if (isset($_SESSION[ 'code'])) { if ($_SESSION[ 'code']=='over_5000char' ) { echo '
							<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",showConfirmButton: false,text:"'.$translator->readKey('over_5000').'"})</script>'; } } $_SESSION['code'] = null; ?></body>

</html>