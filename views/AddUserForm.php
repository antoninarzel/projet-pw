<!DOCTYPE html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
   <![endif]-->
   <!--[if IE 7]>         
   <html class="no-js lt-ie9 lt-ie8">
      <![endif]-->
      <!--[if IE 8]>         
      <html class="no-js lt-ie9">
         <![endif]-->
         <!--[if gt IE 8]>      
         <html class="no-js">
            <!--<![endif]-->
            <html>
               <head>
                  <meta charset="utf-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <title>S'inscrire</title>
                  <meta name="description" content="">
                  <meta name="viewport" content="width=device-width, initial-scale=1">
                  <meta name="robots" content="noindex, nofollow">
                  <link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css">
                  <link rel="stylesheet" href="resources/css/style.css">
                  <link rel="icon" href="resources/images/favicon.ico" />
                  <script src="vendor/components/jquery/jquery.min.js"></script>
                  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                  <script>document.title="<?=$translator->readKey('title_register')?>"</script>
               </head>
               <body>
                  <!--[if lt IE 7]>
                  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
                  <![endif]-->
                  <div class="main">
                  <section class="signup">
                     <div class="container">
                        <div class="signup-content">
                           <div class="signup-form">
                              <h2 class="form-title"><?= $translator->readKey('register_title'); ?></h2>
                              <form method="post" class="register-form" id="register-form">
                                 <div class="form-group">
                                    <label for="firstname"><i class="zmdi zmdi-account-o material-icons-name"></i></label>
                                    <input type="text" minlength="2" maxlength="50" placeholder="<?=$translator->readKey('insert_firstname')?>" name="firstname" id="firstname" autocapitalyze="on" onkeyup='saveValue(this);' required>
                                 </div>
                                 <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>  
                                    <input type="text" minlength="2" maxlength="50" placeholder="<?=$translator->readKey('insert_name')?>" name="name" id="name" onkeyup='saveValue(this);' required>
                                 </div>
                                 <div class="form-group">
                                    <label for="username"><i class="zmdi zmdi-mood"></i></label>  
                                    <input type="text" minlength="2" maxlength="50" placeholder="<?=$translator->readKey('insert_username')?>" name="username" id="username"  pattern="^[a-zA-Z0-9]*$" required>
                                 </div>
                                 <div class="form-group">
                                    <label for="email"><i class="zmdi zmdi-email"></i></label>
                                    <input type="email" minlength="6" maxlength="50" placeholder="<?=$translator->readKey('insert_email')?>" name="email" id="email" required>
                                 </div>
                                 <div class="form-group">
                                    <label for="password"><i class="zmdi zmdi-lock"></i></label>
                                    <input type="password" minlength="8" maxlength="64" placeholder="<?=$translator->readKey('insert_password')?>" name="password" id="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" oninvalid="" required>
                                 </div>
                                 <div class="form-group">
                                     <label for="birthday"><i class="zmdi zmdi-calendar"></i></label>
                                    <input type="date" minlength="2" maxlength="50" min='1899-01-01' name="birthday" id="birthday" required>
                                </div>
                                 <div class="form-group form-button">
                                    <input type="submit" name="submit" id="submit" class="form-submit" value="<?= $translator->readKey('register_title'); ?>"/>
                                 </div>
                              </form>
                           </div>
                           <div class="signup-image">
                              <figure><img src="resources/images/signup-image.jpg" alt="sing up image"></figure>
                              <a href="index.php?page=login" class="signup-image-link"><?= $translator->readKey("already_signed_up"); ?></a>
                           </div>
                        </div>
                     </div>
                  </section>
                </div>
                  
                  
                  <?php 
                     if (isset($_SESSION['code'])) {
                         if ($_SESSION['code'] == 'valid0') {
                            echo '<script>swal({icon:"success",title:"'.$translator->readKey('success_message').'",showConfirmButton: false,text:"'.$translator->readKey('success_register').'"}).then(function(){window.location="index.php?page=login"})</script>';
                         } else if ($_SESSION['code'] == 'err0') {
                           echo '<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",text:"'.$translator->readKey('error_existing_mail_message').'"})</script>';	
                         } else if ($_SESSION['code'] == 'err1') {
                            echo '<script>swal({icon:"error",title:"'.$translator->readKey('error_message').'",text:"'.$translator->readKey('error_existing_username_message').'"})</script>';
                         }
                     } 
                     $_SESSION['code'] = null;
                     ?>
               </body>
            </html>