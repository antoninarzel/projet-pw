<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="robots" content="noindex, nofollow">
  	<link rel="icon" href="resources/images/favicon.ico" />
    <link rel="stylesheet" href="resources/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="resources/css/style.css">
  	<link rel="icon" href="resources/images/favicon.ico" />
    <script src="vendor/components/jquery/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>document.title="<?=$translator->readKey('title_login')?>"</script>
</head>

<body>
    <div class="main">
        <section class="sign-in">
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="resources/images/signin-image.jpg" alt="sing up image"></figure>
                        <a href="index.php?page=register" class="signup-image-link"><?= $translator->readKey('create_account') ?></a>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title"><?= $translator->readKey('login_title') ?></h2>
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" placeholder="<?= $translator->readKey('insert_email') ?>" name="email" id="email" required>
                            </div>
                            <div class="form-group">
                                <label for="password"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" placeholder="<?= $translator->readKey('insert_password') ?>" name="password" id="password" required>
                            </div>
                            <div class="form-group">
                                <a href="index.php?page=recovery" class="signup-image-link"><?= $translator->readKey('password_forgot') ?></a>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signin" id="signin" class="form-submit" value="<?= $translator->readKey('login_title') ?>" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <?php
    if (isset($_SESSION['code'])) {
        if ($_SESSION['code'] == 'valid1') {
            echo '<script>swal({icon:"success",title:"' . $translator->readKey('success_message') . '",showConfirmButton: false,text:"' . $translator->readKey('success_validation') . '"})</script>';
        } else {
            echo '<script>swal({icon:"error",title:"' . $translator->readKey('error_message') . '",text:"' . $translator->readKey('error_validation') . '"})</script>';
        }
    }

	if (isset($_SESSION['err_co_code'])) {
      if ($_SESSION['err_co_code'] == '0') {
        echo '<script>swal({icon:"error",title:"' . $translator->readKey('error_message') . '",text:"'.$translator->readKey('err_notvalid').'"})</script>';
      } else if ($_SESSION['err_co_code'] == '1') {
        echo '<script>swal({icon:"error",title:"' . $translator->readKey('error_message') . '",text:"'.$translator->readKey('err_mail').'"})</script>';
      }
    }

    $_SESSION['code'] = null;
	$_SESSION['err_co_code'] = null;
    ?>
</body>

</html>