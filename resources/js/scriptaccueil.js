let root = document.getElementById("root");


class Fiche{
    constructor(place, title){

        this.place = place;
        this.title = title;

        this.render();
    }

    

    render(){
        this.créerFiche();
        this.place.append(this.ElementFiche);
    }

    créerFiche(){
        
        this.h2 = document.createElement('h2');
        this.h2.innerText = this.title;
        this.div = document.createElement('div');
	this.div.innerText = "résumé fiche"; //récupérer les x premiers caractère de la fiche
        this.ElementFiche = document.createElement('div');
        this.ElementFiche.append(this.h2);
        this.ElementFiche.append(this.div);
        this.ElementFiche.classList.add("Fiche");
    }
}

//Ajouter un élément 
let BouttonAjout = document.getElementById("Ajouter");

BouttonAjout.addEventListener('click',()=>{
   
    var valeur = prompt("Titre de votre fiche");
    if(valeur){
    new Fiche(root, valeur);
    }
   else {
	window.alert("Veuillez ajouter un titre");
	}
	
});



