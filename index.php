<?php
session_start();
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require ('controllers/ApplicationController.php');
require('models/Translator.php');

/* Gestion multilingue du site */
// Langues disponibles : français, anglais, espagnol.
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$acceptLang = ['fr', 'es', 'en']; 
$lang = in_array($lang, $acceptLang) ? $lang : 'fr';
$_SESSION['lang'] = $lang;
$translator = new Translator($lang);
/* */

$controller = ApplicationController::getInstance()->getController($_REQUEST);
if($controller != null){
include "controllers/$controller.php";
(new $controller())->handle($_REQUEST);
}

$view = ApplicationController::getInstance()->getView($_REQUEST);
if($view != null){
include "views/$view.php";
}

?>
